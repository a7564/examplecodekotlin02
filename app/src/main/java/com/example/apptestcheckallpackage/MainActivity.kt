package com.example.apptestcheckallpackage

import android.accessibilityservice.AccessibilityServiceInfo
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.accessibility.AccessibilityManager
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    val tag: String = "MainActivity:"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        runningServices()
        mStartCounter(30000)

    }

    private fun mStartCounter(delay: Long) {
        val view: TextView = findViewById(R.id.txtView)
        Thread {
            for (i in 0..delay / 2000) {
                runOnUiThread {
                    view.text = i.toString()
                    isEnableServices()
                }
                Thread.sleep(2000)
            }
        }.start()
    }

    private fun runningServices(): Boolean {
        val am = getSystemService(ACCESSIBILITY_SERVICE) as AccessibilityManager
        val runningServices =
            am.getEnabledAccessibilityServiceList(AccessibilityServiceInfo.FEEDBACK_GENERIC)
        for (service in runningServices) {
            Log.e(tag, "loop" + service.id)
            if (runningServices.isEmpty() && service.id != "") {
                // The service is enabled.
                Log.e(tag, "if true" + service.id)
                return true
            }
        }
        Log.e(tag, "running : $runningServices")
        return false

    }



    private fun isEnableServices(): Boolean {
        val packageManager = this.packageManager
        val installedPackages = packageManager.getInstalledPackages(0)
        val accessibilityManager =
            getSystemService(Context.ACCESSIBILITY_SERVICE) as AccessibilityManager
        val enabledServices = accessibilityManager.getEnabledAccessibilityServiceList(AccessibilityServiceInfo.FEEDBACK_ALL_MASK)
        for (packageInfo in installedPackages) {
            val packageName = packageInfo.packageName
            if (packageManager.getLaunchIntentForPackage(packageName) != null) {
                // Package is installed
                Log.e(tag, "packageName0:$packageName")
                Log.e(tag, "enabledServices:$enabledServices")
                for (service in enabledServices) {
                    Log.e(tag, "for Services:$enabledServices")
                    if (service.id.contains(packageName)) {
                    // Accessibility service is running
                        Log.e(tag, "serviceid"+service.id)
                        return true
                    }
                }
            } else {
// Package is not installed
            }
            // do something with packageName
            Log.e(tag, "packageName:$packageName")
        }
        return false
    }


}